import json
from web3 import Web3

ganache_url = "http://192.168.25.190:8545"
web3 = Web3(Web3.HTTPProvider(ganache_url))

if web3.isConnected():
    print('connected')
else: 
    print('not connected')

mining = web3.eth.mining
print(mining)