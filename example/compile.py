from solc import compile_files
 # Compile all contract files
contracts = compile_files(['user.sol', 'stringUtils.sol'])
 # separate master file and link file
main_contract = contracts.pop("user.sol:userRecords")
library_link = contracts.pop("stringUtils.sol:StringUtils")