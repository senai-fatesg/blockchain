pragma solidity ^0.5.0;

contract financiamentoContract {

    uint saldo = 400000;

    function getSaldo() public view returns (uint){
        return saldo;
    }

    function depositar(uint dep) public {
        saldo = saldo + dep;
    }
}