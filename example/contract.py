import json
from web3 import Web3

ganache_url = "http://localhost:7545"
web3 = Web3(Web3.HTTPProvider(ganache_url))

if web3.isConnected():
    print('connected')
else: 
    print('not connected')

web3.eth.defaultAccount = web3.eth.accounts[2]

abi = json.loads('[{"constant":false,"inputs":[{"name":"dep","type":"uint256"}],"name":"depositar","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"getSaldo","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"}]')
bytecode = '608060405262061a8060005534801561001757600080fd5b5060f0806100266000396000f3fe6080604052600436106049576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff168063d5ca622814604e578063e2eec9a7146085575b600080fd5b348015605957600080fd5b50608360048036036020811015606e57600080fd5b810190808035906020019092919050505060ad565b005b348015609057600080fd5b50609760bb565b6040518082815260200191505060405180910390f35b806000540160008190555050565b6000805490509056fea165627a7a7230582019c606d7eba04d56b605ae3670b006c11921b3615830875af84d6355cc0612620029'

Greeter = web3.eth.contract(abi=abi, bytecode=bytecode)

tx_hash = Greeter.constructor().transact()
print(tx_hash)

tx_receipt = web3.eth.waitForTransactionReceipt(tx_hash)

contract = web3.eth.contract(
    address=tx_receipt.contractAddress,
    abi=abi
)

print(contract.functions.getSaldo().call())

tx_hash = contract.functions.depositar(1000).transact()
tx_receipt = web3.eth.waitForTransactionReceipt(tx_hash)
print(contract.functions.getSaldo().call())