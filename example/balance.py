import json
from web3 import Web3

# ganache_url = "http://localhost:7545"
ganache_url = "http://localhost:8080"
web3 = Web3(Web3.HTTPProvider(ganache_url))

if web3.isConnected():
    print('connected')
else: 
    print('not connected')

balance = web3.eth.getBalance(web3.eth.accounts[0])
print(balance)