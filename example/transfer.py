import json
from web3 import Web3

ganache_url = "http://localhost:7545"
web3 = Web3(Web3.HTTPProvider(ganache_url))

if web3.isConnected():
    print('connected')
else: 
    print('not connected')

tx_dict = {'from': web3.eth.accounts[0], 'to': web3.eth.accounts[1], 'value': 1000000000000000000}

web3.eth.sendTransaction(tx_dict)